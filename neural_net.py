
'''Trains a simple convnet on the MNIST dataset.
Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from scipy.misc import imread, imresize
from keras.models import model_from_json, load_model
import scipy as sp
import numpy as np
import random
import os
from progress.bar import Bar
 
"This class will allow for trained neural networks to be run on images that are sent optically"
class neural_network():
    
    def __init__(self,image,model_file):
        self.image = image
        self.model_folder = 'nn_model/'
        self.model_file = model_file
    
    def run_model_on_image(self,new_weights=False):
        x = self.image
        #compute a bit-wise inversion so black becomes white and vice versa
        # x = np.invert(x)
        #make it the right size
        x = sp.misc.imresize(x,(28,28))
        #convert to a 4D tensor to feed into our model
        x = x.reshape((1,28*28))
        x = x[::4,::4].reshape((1,14*14))
        x = x.astype('float32')
        x /= 255

        model = load_model(self.model_folder + self.model_file + '.h5')
        if(new_weights != False):
            model.set_weights(new_weights)
        out = model.predict(x)
        print(np.argmax(out))
        return np.argmax(out)

    def batch_predict(self,to_predict,shape=(28,28),new_weights=False):
        model = load_model(self.model_folder + self.model_file + '.h5')
        if(new_weights != False):
            model.set_weights(new_weights)
        correct_count = 0
        bar = Bar("Testing MNIST on ONN",max=len(to_predict))
        for index,i in enumerate(to_predict):
            mnist_image , numberOfImage = load_mnist_image(number=i)
            x = sp.misc.imresize(mnist_image,(shape[0],shape[1]))

            x = x.reshape((1,shape[0]*shape[1]))
            x = x.astype('float32')
            x /= 255

            out = model.predict(x)
            prediction = np.argmax(out)
            if(prediction == i):
                correct_count += 1
            bar.next()
        bar.finish()
        return correct_count

    def load_weights_from_model(self):
        model = load_model(self.model_folder + self.model_file + '.h5')
        weights = model.get_weights()
        return weights

    def load_weights_from_model_8bit(self):
        model = load_model(self.model_folder + self.model_file + '.h5')
        weights = model.get_weights()
        weightlist8bit = []
        for i in weights:
            weightlist8bit.append(i.astype("float16"))
        return weightlist8bit

    def train_model(self):
        batch_size = 128
        num_classes = 10
        epochs = 30

        # input image dimensions
        img_rows, img_cols = 14, 14

        # the data, shuffled and split between train and test sets
        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        def resize_xs(xs):
            N,dim = xs.shape
            if dim != 28*28:
                print('size wrong', dim)
                return
            xs_new = np.zeros((N, img_rows*img_cols),dtype='float32')
            for i in range(N):
                xi = xs[i].reshape(28,28)
                temp = sp.misc.imresize(xi,size=(img_rows,img_cols),interp='cubic')
                xs_new[i] = temp.reshape(img_rows*img_cols)
            return xs_new

        x_train = x_train.reshape(60000, 784)
        x_test = x_test.reshape(10000, 784)
        x_train = resize_xs(x_train)
        x_test = resize_xs(x_test)

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')

        # convert class vectors to binary class matrices
        y_train = keras.utils.to_categorical(y_train, num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

        model = Sequential()
        model.add(Dense(32, activation='relu', input_shape=(img_rows*img_cols,)))
        model.add(Dense(num_classes, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer=keras.optimizers.Adadelta(),
                    metrics=['accuracy'])

        model.fit(x_train, y_train,
                batch_size=batch_size,
                epochs=epochs,
                verbose=1,
                validation_data=(x_test, y_test))
        score = model.evaluate(x_test, y_test, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        model.save(self.model_folder + self.model_file + '.h5')

def load_mnist_image(number=9):
    choice = random.choice(os.listdir("mnist_images/training/" + str(number)))
    image = sp.misc.imread("mnist_images/training/" + str(number) + "/" + choice , mode='L')
    return image,number

if __name__ == "__main__":
    # pass
    image,number = load_mnist_image(number=6)
    neuralNetwork = neural_network(image=image,model_file="model")
    neuralNetwork.train_model()
    neuralNetwork.load_weights_from_model()
    # neuralNetwork.run_model_on_image()
    to_predict = [int(i/(1000/10)) for i in range(1000)]
    weights = neuralNetwork.load_weights_from_model_8bit()
    correct_count = neuralNetwork.batch_predict(shape=(14,14),to_predict=to_predict,new_weights=weights)
    print("Accuracy: ", correct_count/1000 * 100, "%")

