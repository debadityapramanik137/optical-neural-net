from imutils.video import FPS
# from imutils.video.pivideostream import PiVideoStream
from picamera import PiCamera
from threading import Thread
from picamera.array import PiRGBArray
import imutils
import time
import cv2
import imageio
import numpy as np
from io import BytesIO
import picamera
from PIL import Image


class camera():
    def __init__(self):
        # self.resolutionx = 2592
        # self.resolutiony = 1944
        self.resolutionx = 1920
        self.resolutiony = 1080
        self.upleft = (605,251)
        self.downleft = (622,617)
        self.downright = (1340,586)
        self.upright = (1328,221)

    
    def take_image(self):
        stream = BytesIO()
        camera = picamera.PiCamera()
        camera.resolution = (self.resolutionx, self.resolutiony)
        camera.framerate = 15
        camera.awb_mode = 'off'
        camera.awb_gains = (0.2,1.0)
        # camera.start_preview()
        camera.led = False
        time.sleep(0.3)
        camera.capture(stream, format='jpeg',use_video_port=True)
        stream.seek(0)
        returnable_image = Image.open(stream)
        # camera.stop_preview()
        camera.close()
        return returnable_image

if __name__ == "__main__":
    pass
    # camera = camera()
    # camera.take_image()

