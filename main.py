#!/usr/bin/env python
from PIL import Image, ImageDraw, ImageFont
import cv2
from io import BytesIO
import numpy as np
import scipy as sp
from skimage import feature
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
from skimage import measure
from skimage.color import rgb2gray
from math import sqrt
import time
from gpiozero import LED
import sys
import struct
import pickle
from camera import camera 
from display import display
from progress.bar import Bar
from neural_net import load_mnist_image
from neural_net import neural_network
import imageio
plt.rcParams['figure.figsize'] = [20, 10]

import os 
        
display = display()

camera = camera()    




class grid():
    def __init__(self,shape=(362,727)):
        self.number_of_dots_in_x_axis = 32
        self.number_of_dots_in_y_axis = 32
        self.x_offset_left = 7
        self.x_offset_right = 14
        self.y_offset_up = 8
        self.y_offset_down = 4
        self.grid = self.generate_grid(shape=shape,number_of_dots_in_x_axis=self.number_of_dots_in_x_axis,number_of_dots_in_y_axis = self.number_of_dots_in_y_axis,
            x_offset_left=self.x_offset_left,x_offset_right=self.x_offset_right,y_offset_up=self.y_offset_up,y_offset_down = self.y_offset_down)

    '''
    Runs a test that shows if the grid is alligned. If the grid is not alligned the program terminates and the file grid.png can be viewed to see the allignment.
    '''
    def test_grid_allignment(self,shape=(32,32)):
        led = LED(21)
        led.off()
        image_utils_class = image_utils()
        sample_array = image_utils_class.generate_sample_array_binary()
        measured_array_off, led_value_off = image_utils_class.extract_matrix_from_binary_array(dot_shape = shape, detect_led = True ,binary_array= sample_array,savefig=True)
        if(np.array_equal(measured_array_off,np.ones(shape))):
            print("The grid is alligned, led off")
        else:
            print("led off grid not alligned")
            sys.exit()
        if(led_value_off==0):
            print("Led off correctly identified")
        else:
            print("Led off not correctly identified")
            sys.exit()

        led.on()
        measured_array_on, led_value_on = image_utils_class.extract_matrix_from_binary_array(dot_shape = shape, detect_led = True ,binary_array= sample_array,savefig=True)
        if(np.array_equal(measured_array_on,np.ones(shape))):
            print("The grid is allignment, led on")
        else:
            print("Led on grid not alligned")
            sys.exit()
        if(led_value_on==1):
            print("Led on Correctly identified")
        else:
            print("Led on not correctly identified")
            sys.exit()

    '''
    Given the function arguments it crafts a grid which is used to find pixel values
    '''
    def generate_grid(self,shape,number_of_dots_in_x_axis,number_of_dots_in_y_axis,x_offset_left,x_offset_right,y_offset_up,y_offset_down):
        y = shape[0]
        x = shape[1]
        y_usable = y - y_offset_up - y_offset_down
        x_usable = x - x_offset_right - x_offset_left
        y_delta = y_usable/(number_of_dots_in_y_axis-1)
        x_delta = x_usable/(number_of_dots_in_x_axis-1)
        dot_coordinates = np.zeros((number_of_dots_in_x_axis,number_of_dots_in_y_axis),dtype=object)
        for x_coor in range(number_of_dots_in_x_axis):
            for y_coor in range(number_of_dots_in_y_axis):
                dot_coordinates[x_coor,y_coor] = [x_offset_left + x_coor*x_delta,y_offset_up + y_coor*y_delta]
        return dot_coordinates
    
    '''
    Given the image and the grid it maps the grid onto the image and returns a numpy array representing the matrix of values on the screen.
    '''
    def get_binary_values_from_image_with_grid(self,image,grid):
        returnable = np.zeros(grid.shape)
        for x in range(grid.shape[1]):
            for y in range(grid.shape[0]):
                grid_coordinate = grid[y,x]
                pixel_value = image[int(grid_coordinate[1]),int(grid_coordinate[0])]
                if(pixel_value > 60):
                    returnable[y,x] = 1
                else:
                    returnable[y,x] = 0
        return returnable

gridclass = grid(shape = (362,727))


class image_utils():
    def importImageFromFile(self,filename):
        image = Image.open(filename)
        return image

    def crop_image(self,image, crop_coordinates):
        cropped_image = image.crop(crop_coordinates)
        return cropped_image

    def return_color_channel(self,image,channel):
        red, green, blue =  image.split()
        if(channel == "red"):
            return red
        elif(channel == "blue"):
            return blue
        elif(channel == "green"):
            return green
        else:
            print("Error, channel is not red green or blue")

    '''
    Generates a grid of dots which is 32x32 with zeros inbetween for testing the grid alignment
    '''
    def generate_sample_array_binary(self):
        sample_array = np.zeros((display.height,display.width),dtype=np.uint8)
        for y in range(sample_array.shape[0]):
            for x in range(sample_array.shape[1]):
                #The display is 128 by 64, so if we want 32x32 we need to step through 4 at a time on the x axis etc
                if(x%4==0 and y%2==0):
                    sample_array[y,x] = 1
        return sample_array

    '''
    This method takes an array and then runs it into a binary uint8 array that is of size display.height x display.width
    '''
    def fill_area_with_zeros(self,new_shape,array):
        returnable = np.zeros((display.height,display.width),dtype=np.uint8)
        y_delta = int(display.height/new_shape[0])
        x_delta = int(display.width/new_shape[1])
        for x in range(display.width):
            for y in range(display.height):
                if(x%x_delta==0 and y%y_delta==0):
                    if(array[int(y * new_shape[0]/display.height),int(x * new_shape[1]/display.width)]== 1):
                        returnable[y,x] = 255
        return returnable

    '''
    Takes a numpy array representing an image and creates a new array by padding the original with zeros until it is 32x32
    '''
    def pad_image_to_32by32(self,image):
        returnable = np.zeros((32,32))
        returnable[:image.shape[0],:image.shape[1]] = image
        return returnable

    '''
    Orders points for the four_point_transform
    '''
    def order_points(self,pts):
        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype = "float32")

        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]

        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]

        # return the ordered coordinates
        return rect

    '''
    Warp the image so that the four corners are in the corners of the image.
    '''
    def four_point_transform(self,image, pts):
        # obtain a consistent order of the points and unpack them
        # individually
        rect = self.order_points(pts)
        (tl, tr, br, bl) = rect

        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype = "float32")

        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

        # return the warped image
        return warped
    
    def extract_matrix_from_binary_array(self,dot_shape,binary_array,detect_led=False,savefig=False,grid=False):
        testArray = self.fill_area_with_zeros(new_shape=binary_array.shape,array=binary_array)
        display.set_leds(testArray)
        image_string = camera.take_image()
        if(detect_led==True):
            led_boolean = self.detect_led_presence(image_string)
        blue_image = self.return_color_channel(image_string,'blue')
        im = np.array(blue_image,dtype = np.uint8)
        
        points = np.array([camera.upleft,camera.downleft,camera.upright,camera.downright])
        warped_image = self.four_point_transform(im,points)
        
        grid = gridclass.grid
        #We are going to get the binary values from the image that correspond to the grid
        binary_grid_values = gridclass.get_binary_values_from_image_with_grid(warped_image,grid)

        #transpose this to compersate for it not being the right dimensions
        binary_grid_values = binary_grid_values.T

        if(savefig==True):
            blue_image.save("images/blue_image_grid_test.png",'PNG')
            fig, axes = plt.subplots(figsize=(20, 10))
            plt.imshow(warped_image,interpolation='nearest')

            for line in grid:
                for point in line:
                    x,y = point
                    c = plt.Circle((x, y), 0.4, color='red', linewidth=2, fill=False)
                    axes.add_patch(c)

            axes.set_axis_off()
            start_time = time.time()
            plt.savefig("images/grid.png")
            print("Time to save grid image",time.time()-start_time)
        if(detect_led==False):
            return binary_grid_values
        else:
            return binary_grid_values,led_boolean

    def detect_led_presence(self,image_string):
        red_image = self.return_color_channel(image_string,'red')
        red_image.save("red_led_image.png",'PNG')
        red_image_array = np.array(red_image)
        value = red_image_array[158,1210]
        if(value > 90):
            return 1
        else:
            return 0

image_utils_class = image_utils()


class communication_handler():
    def __init__(self,image=np.zeros((2,2))):
        self.image = image
        self.height = self.image.shape[0]
        self.width = self.image.shape[1]

    def better_binary_repr(self,pixel_value,num_bits=8):
        #if the value that is being passed in is basically a int then pass the binary of the int (this is a string)
        if(num_bits==16):
            return bin(np.float16(pixel_value).view('H'))[2:].zfill(16)
        if(int(pixel_value)-pixel_value == 0):
            return np.binary_repr(pixel_value.astype(int),width=num_bits)
        else:
            #if we have a value which is meant to be floats
            return ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', pixel_value))

    def better_pack_bits(self,topack):
        if(len(topack)==8):
            return np.packbits(topack.astype(int))
        if(len(topack)==16):
            topackstring = ''.join([str(int(x)) for x in topack])
            a=struct.pack("H",int(topackstring,2))
            return np.frombuffer(a, dtype = np.float16)[0]
        else:
            string_topack = ''.join(str(int(x)) for x in topack)
            f = int(string_topack, 2)
            packed = struct.unpack('f', struct.pack('I', f))[0]
            return packed

    def binarystringtoarray(self,binarystring):
        returnable = []
        for i in binarystring:
            returnable.append(int(i))
        return np.array(returnable)

    def decode_image(self,num_bits=8):
        #We are going to create a list of arrays which we will pipe into through the system one at a time
        decoded_image = np.zeros((self.height,self.width,num_bits))
        for x in range(self.width):
            for y in range(self.height):
                binary_representation = self.better_binary_repr(self.image[y,x],num_bits=num_bits)
                # binary_representation = np.binary_repr(self.image[y,x],width=num_bits)
                binary_representation = self.binarystringtoarray(binary_representation)
                decoded_image[y,x] = binary_representation
        return decoded_image

    def encode_image(self,array):
        returnable = np.zeros((array.shape[0],array.shape[1]))
        for x in range(array.shape[1]):
            for y in range(array.shape[0]):
                value = self.better_pack_bits(array[y,x,:])
                # value = np.packbits(array[y,x,:].astype(int))
                returnable[y,x] = value
        return returnable
                
    def send_images(self,num_bits=8):
        decoded_image = self.decode_image(num_bits=num_bits)
        returned_matrix_storage = np.zeros((self.height,self.width,num_bits))
        grid = gridclass.grid
        for i in range(num_bits):
            returned_matrix = image_utils_class.extract_matrix_from_binary_array(dot_shape = (self.height,self.width) ,binary_array = decoded_image[:,:,i],grid=grid) 
            returned_matrix_storage[:,:,i] += np.fliplr(np.flipud(returned_matrix))
        return self.encode_image(returned_matrix_storage)

    def send_images_with_led(self,led_value,num_bits=8):
        led = LED(21)
        #convert led value in num_bits of binary
        binary_led = self.better_binary_repr(led_value,num_bits)
        binary_led = self.binarystringtoarray(binary_led)
        decoded_image = self.decode_image(num_bits=num_bits)
        returned_matrix_storage = np.zeros((self.height,self.width,num_bits))
        grid = gridclass.grid
        for i in range(num_bits):
            if(binary_led[i]==0):
                led.off()
            elif(binary_led[i]==1):
                led.on()
            else:
                print("binary led value is not zero or one")
            returned_matrix, sent_led = image_utils_class.extract_matrix_from_binary_array(dot_shape = (self.height,self.width), detect_led=True, binary_array = decoded_image[:,:,i],grid=grid) 
            returned_matrix_storage[:,:,i] += np.fliplr(np.flipud(returned_matrix))
        led.close()
        return self.encode_image(returned_matrix_storage) , sent_led

    def filter_image(self,image,filter_shape):
        return image[:filter_shape[0],:filter_shape[1]]

    def result_checker(self,originalarray,finalarray,tolerance=False):
        if(np.array_equal(originalarray,finalarray)):
            return True
            # print("The original and sent matricies are equal") 
        elif(tolerance != False and np.allclose(originalarray,finalarray,atol=tolerance)):
            return True
        else:
            print("The original and sent matricies are not equal")
            number_of_true = np.sum(np.isclose(originalarray,finalarray))
            total_number = originalarray.size
            accuracy = float(number_of_true * 100.0 / total_number)
            print("The accuracy is : " + str(accuracy),'%')
            difference_array = np.abs(np.subtract(finalarray,originalarray))
            try:
                sp.misc.imsave("images/difference_array.png",difference_array)
            except:
                pass
            return False

class transfer_data():
    '''
    Takes an image, pads it to a 32x32 image and sends it. Returns the transfered image
    '''
    def transfer_image(self,image,check_results=True):
        padded_weight_image = image_utils_class.pad_image_to_32by32(image)
        communication = communication_handler(padded_weight_image)
        sent_image = communication.send_images(num_bits=8).astype(int)
        if(check_results):
            communication.result_checker(padded_weight_image,sent_image)
        return sent_image

    '''
    Takes weights from the neural network and transfers them
    '''
    def transfer_weight(self,weights):
        if(weights.shape[0]==784):
            layer_object = np.zeros((784,10))
        elif(weights.shape[0]==10):
            layer_object = np.zeros((10,10))
        else:
            print("layer object not created")
        bar = Bar("Transfering layer weight",max=weights.shape[1])
        for weight in range(weights.shape[1]):
            if(weights[:,weight].shape[0]==784):
                resized_layer = np.resize(weights[:,weight],(28,28))
                filter_shape = (28,28)
            elif(weights[:,weight].shape[0]==10):
                resized_layer = np.resize(weights[:,weight],(10,1))
                filter_shape = (10,1)
            else:
                print("Layer size is incorrect")
            padded_weight_image = image_utils_class.pad_image_to_32by32(resized_layer)
            communicationweight = communication_handler(padded_weight_image)
            sent_weight = communicationweight.send_images(num_bits=32)
            communicationweight.result_checker(padded_weight_image,sent_weight)
            filter_sent_weight = communicationweight.filter_image(sent_weight,filter_shape)
            layer_object[:,weight] = np.reshape(filter_sent_weight, (weights.shape[0]))
            bar.next()
        bar.finish()
        return layer_object

    '''
    Takes biases from the neural network and transfers them
    '''
    def transfer_biases(self,biases):
        print(biases.shape)
        bar = Bar("Transfering layer biases", max=1)
        resized_layer = np.resize(biases,(len(biases),1))
        filter_shape = (print(len(biases)),1)
        padded_weight_image = image_utils_class.pad_image_to_32by32(resized_layer)
        communicationweight = communication_handler(padded_weight_image)
        sent_weight = communicationweight.send_images(num_bits=32)
        communicationweight.result_checker(padded_weight_image,sent_weight)
        filter_sent_weight = communicationweight.filter_image(sent_weight,filter_shape)
        layer_object = np.reshape(filter_sent_weight, (filter_sent_weight.shape[0],))
        bar.next()
        bar.finish()
        return layer_object

    def convert_to_float32(self,inputnumber):
        return ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', inputnumber))

    '''
    Inputs:
    weights : numpy array of size layer[n].size x layer[n+1].size. For example, for first mnist layer this is 784 x 10
    image : 
    Outputs:
    The sent weights and images
    '''
    def transfer_weight_with_led_being_image(self,weights,image):
        flattened_image = np.resize(image,(image.size,))
        returnable_weights = np.zeros(weights.shape)
        returnable_image = np.zeros(flattened_image.shape)
        bar = Bar("Transfering pixel and weight", max=len(flattened_image))
        for pixelindex, pixel in enumerate(flattened_image):
            weightList = weights[pixelindex]
            weightarray = self.weightList_to_weightarray(weightList)
            padded_weight_image = image_utils_class.pad_image_to_32by32(weightarray)
            communicationweight = communication_handler(padded_weight_image)
            sent_weight,sent_led = communicationweight.send_images_with_led(led_value = pixel ,num_bits=16)
            # sent_weight = communicationweight.send_images(num_bits=16)
            # returnable_image[pixelindex] += pixel
            returnable_image[pixelindex] = sent_led
            communicationweight.result_checker(padded_weight_image,sent_weight,tolerance=0.001)
            filter_sent_weight = communicationweight.filter_image(weightarray,weightarray.shape)
            returnable_weights[pixelindex] += np.reshape(filter_sent_weight,(filter_sent_weight.size,))
            bar.next()
        bar.finish()
        return returnable_weights,returnable_image

    def weightList_to_weightarray(self,weightList):
        return np.reshape(weightList,(weightList.shape[0],1))

    def apply_activation_function_to_sent_weight_and_image(self,sent_weight,sent_image,biases,activation_function = "relu"):
        next_layer = np.zeros(biases.shape[0])
        for pixelindex,pixel in enumerate(sent_image):
            #pixel * w + b 
            next_layer = np.add(pixel * sent_weight[pixelindex,:] + biases,next_layer)
        if(activation_function=="relu"):
            return next_layer * (next_layer > 0)
        elif(activation_function=="sigmoid"):
            return 1./(1 + np.exp(next_layer))
        elif(activation_function=="softmax"):
            e_x = np.exp(next_layer - np.max(next_layer))
            return e_x / e_x.sum(axis=0)
        else:
            print("Activation function is not available")

    def transfer_and_run_neural_network_layer(self,image,weights,bias,activation_function='relu'):
        sent_weights, sent_flattened_image = self.transfer_weight_with_led_being_image(weights,image)
        layer_output = self.apply_activation_function_to_sent_weight_and_image(sent_weight=sent_weights,sent_image=sent_flattened_image,biases=bias,activation_function=activation_function)
        return layer_output

transfer = transfer_data()

def test_accuracy_of_transfered_weights(number_to_test_on=1000): 
    with open('sent_weights/sent.txt','rb') as weight_file:
        weights = pickle.load(weight_file)
        #itterate over 10,000 range mnist images
        numbers_to_predict = [int(i/(number_to_test_on/10)) for i in range(number_to_test_on)]
        neuralNetwork = neural_network(image=np.zeros((2,2)),model_file="model")
        number_correct_guessed = neuralNetwork.batch_predict(shape=(14,14),to_predict=numbers_to_predict,new_weights=weights)
        print("Accuracy is :", number_correct_guessed/number_to_test_on *100, '%')

def main():
    print("Testing grid allignment")
    gridclass.test_grid_allignment()
    print("Sending image for neural network")
    mnist_image , numberOfImage = load_mnist_image(number=3)
    mnist_image = sp.misc.imresize(mnist_image,(14,14))
    # sent_image = transfer.transfer_image(mnist_image)
    #check if the original and new arrays are close to each other
    neuralNetwork = neural_network(image=np.zeros((2,2)),model_file="model")
    #weight transfer
    weightList = neuralNetwork.load_weights_from_model()
    transferedWeightList = []
    print("Print weight shapes")
    for i in weightList:
        print(i.shape)
    print("Done printing shapes")

    input_layer_weights = weightList[0]
    middle_layer_bias = weightList[1]
    middle_layer_weights = weightList[2]
    output_layer_biases = weightList[3]
    middle_layer_input = transfer.transfer_and_run_neural_network_layer(mnist_image,input_layer_weights,middle_layer_bias,'relu')
    output_layer_output = transfer.transfer_and_run_neural_network_layer(middle_layer_input,middle_layer_weights,output_layer_biases,'softmax')
    print(output_layer_output)
    print("Guess is:", np.argmax(output_layer_output))
  
    #check that the weight values that have been transfered are similar
    print("Checking if the transfered weights are equal")
    for i in range(len(transferedWeightList)):
        communicationweight = communication_handler(np.zeros((2,2)))
        communicationweight.result_checker(weightList[i],transferedWeightList[i])
    
    #save the weights to a file for later testing and use
    with open('sent_weights/sent.txt','wb') as weight_file:
        pickle.dump(transferedWeightList,weight_file)

if __name__ == "__main__":
    main()
    test_accuracy_of_transfered_weights()
